# HackerNews

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.0.

## App information

App displays HackerNews article links and user comments.  
Comments are viewed via `expand/collapse` button instead in new page.  
Comment counter works only after expanding comments for the first time. Counter couldn't be implemented differently because it needs to count each comment request after it was triggered, and triggering all requests at the same time could lead to massive wait time (possible app crash).  
Navigation links disabled because of API limitations.  
  
Project uses official [Hacker News API](https://github.com/HackerNews/API)
## Installation guide: 

- Node.js [install guide](https://nodejs.org/en/download/package-manager/)
- Angular 5 [install guide](https://angular.io/guide/quickstart)
- For clone of project, run ```git clone https://gitlab.com/racko92/hacker-news.git``` for `HTTPS`, or ```git clone git@gitlab.com:racko92/hacker-news.git``` if you prefer `SSH`
- Run ```npm install``` when enter downloaded directory to install dependencies specified in `package.json`
- To run local development server, run `ng serve --open` to open page in default browser, or `ng serve` and then go to `http://localhost:4200/`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
