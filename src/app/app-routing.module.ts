import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TopStoriesComponent } from './components/top-stories/top-stories.component';


const appRoutes: Routes = [
    {
        path: '',
        redirectTo: 'top-stories',
        pathMatch: 'full'
    },
    {
        path: 'top-stories',
        component: TopStoriesComponent,
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes
        )
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {}
