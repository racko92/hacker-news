import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { TopStoriesComponent } from './components/top-stories/top-stories.component';
import { CommentsComponent } from './components/comments/comments.component';
import { SharedModule } from './shared/shared.module';

import { MomentModule } from 'angular2-moment';

@NgModule({
  declarations: [
    AppComponent,
    TopStoriesComponent,
    CommentsComponent,
  ],
  imports: [
    BrowserModule,
    MomentModule,
    AppRoutingModule,
    HttpModule,
    SharedModule,
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
