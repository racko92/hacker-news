import { Component, ElementRef } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'app';
  today: number = Date.now();

  // Gets DOM elements by ID and changes appearance by adding and removing classes
  constructor(private elRef: ElementRef) {}
  triggerMenu(event) {
    const nav = document.getElementById('nav-links');
    const button = document.getElementById('menu-trigger');

    if (nav.classList.contains('nav-links')) {
      nav.classList.remove('nav-links');
      nav.classList.add('nav-links-hidden');
      button.classList.add('is-active');
    } else {
      nav.classList.add('nav-links');
      nav.classList.remove('nav-links-hidden');
      button.classList.remove('is-active');

    }
  }
}
