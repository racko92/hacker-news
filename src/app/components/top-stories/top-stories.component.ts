import { Component, OnInit } from '@angular/core';
import { TopStoriesService } from '../../shared/services/top-stories.service';
import { ActivatedRoute } from '@angular/router';
import { ITopStory } from './../../shared/interfaces/ITopStory';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-top-stories',
  templateUrl: './top-stories.component.html',
  styleUrls: ['./top-stories.component.css']
})
export class TopStoriesComponent implements OnInit {

  public topStories: ITopStory[] = [];
  public topStoriesIdArray: number[];
  public topStoriesCommentsCounter: number[];
  public error: any;
  public startOfStories = 0;
  public storiesToLoad = 30;

  constructor(
    private topStoriesService: TopStoriesService
  ) { }

  ngOnInit() {

    // Subscribes to Observable in service that gets array id's of up to 500 top-stories
    this.topStoriesService.getTopStoriesIds().subscribe(
      (res) => {
        this.topStoriesIdArray = res;
        this.getTopStories();
      },
      (err) => {
        this.error = err;
      });
  }

  // Slices id's array for first 30, and sends them to service function that returns observable with corresponding stories JSON
  getTopStories() {
    this.topStoriesCommentsCounter = [];
    this.topStoriesIdArray.slice(this.startOfStories, this.storiesToLoad).map(
      (id) => {
        this.topStoriesService.getItem(id).subscribe(
          (story) => {
            this.topStories.push(story);
          },
          (error) => {
            console.log(error);
          });
      });
  }

  // Triggered in UI when expanding comments
  // Function checks if comments are loaded previously, shows them if true, gets them if false
  getStoryComments(story) {
    const index = this.topStories.indexOf(story);
    if (!this.topStoriesCommentsCounter[index]) {
      this.topStoriesCommentsCounter[index] = 0;
    }
    if (this.topStories[index].comments) {
      if (this.topStories[index].commentsLoaded) {
        this.topStories[index].commentsLoaded = false;
      } else {
        this.topStories[index].commentsLoaded = true;
      }
    } else {
      this.topStories[index].commentsLoaded = true;
      this.recursiveComments(story, index);
    }
  }

  // Checks if passed parameter has property 'kids', loads them from service if true
  // If aquired 'kid' has parameter 'kids' function calls itself recursively to gather all children elements
  recursiveComments(item, index) {
    if (item.kids) {
      item.kids.map(
        (kidId) => {
          this.topStoriesCommentsCounter[index] += 1;
          this.topStoriesService.getItem(kidId).subscribe(
            (kid) => {
              if (!kid.deleted) {
                if (kid.kids) {
                  this.recursiveComments(kid, index);
                }
                if (!item.comments) {
                  item.comments = [];
                }
                item.comments.push(kid);
                return item;
              }
            },
            (error) => {
              console.log(error);
            }
          );
        }
      );
    } else {
      this.topStoriesCommentsCounter[index] += 1;
    }
    this.topStories[index].commentLength = this.topStoriesCommentsCounter[index];
  }

  // Triggered with 'Load more' button to aquire 30 more stories that are pushed to existing topStories
  loadMoreStories() {
    this.startOfStories = this.storiesToLoad;
    this.storiesToLoad += 30;
    this.getTopStories();
    console.log(this.topStories);
  }

  // Smothly returns page to top when triggered from 'Back to top' button
  backToTop() {
    document.body.scrollIntoView({behavior: 'smooth', block: 'start'});
  }
}
