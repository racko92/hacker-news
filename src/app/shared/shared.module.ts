import { NgModule } from '@angular/core';

import { ScrollHeightDirective } from './directives/scroll-height.directive';
import { TopStoriesService } from './services/top-stories.service';
import { ProcessHttpmsgService } from './services/process-httpmsg.service';

@NgModule({
  declarations: [
    ScrollHeightDirective,
  ],
  exports: [
    ScrollHeightDirective,
  ],
  providers: [
    ProcessHttpmsgService,
    TopStoriesService,
  ]
})
export class SharedModule { }
