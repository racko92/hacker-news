import { Directive, HostListener, ElementRef } from '@angular/core';

@Directive({ selector: '[scrollHeight]' })
export class ScrollHeightDirective {

  private previousScroll: number;
  private scrollCounter = 0;

   constructor(private elRef: ElementRef) {
   }

   @HostListener('window:scroll', ['$event'])

  // Directive for navigation DOM element that listens for page scroll event
  // Changes navigation style that depends of window width, Y offset, and scroll direction
  onScrollEvent($event) {
    if (window.pageYOffset >= 20 && window.innerWidth > 800) {
    this.elRef.nativeElement.classList.remove('nav-bar');
    this.elRef.nativeElement.classList.add('nav-bar-scroll');
    } else if (window.pageYOffset >= 20 && window.innerWidth < 800) {
      if (this.previousScroll > window.pageYOffset) {
          this.scrollCounter += 1;
        } else if (this.previousScroll < window.pageYOffset) {
          this.scrollCounter = 0;
        }

        this.previousScroll = window.pageYOffset;

        if (this.scrollCounter > 10) {
          this.elRef.nativeElement.classList.remove('nav-bar');
          this.elRef.nativeElement.classList.add('nav-bar-scroll-top');
          return;
        } else {
          this.elRef.nativeElement.classList.add('nav-bar');
          this.elRef.nativeElement.classList.remove('nav-bar-scroll-top');
          return;
        }
    } else {
    this.elRef.nativeElement.classList.add('nav-bar');
    this.elRef.nativeElement.classList.remove('nav-bar-scroll');
    this.elRef.nativeElement.classList.remove('nav-bar-scroll-top');
    }
  }
}
