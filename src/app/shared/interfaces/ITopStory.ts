export interface ITopStory {
    id?: number;
    by?: string;
    descendants?: number;
    kids?: number[];
    score?: number;
    time?: number;
    title?: string;
    type?: string;
    url?: string;
    deleted?: boolean;
    text?: string;
    dead?: boolean;
    parent?: number;
    comments?: ITopStory[];
    commentLength?: number;
    commentsLoaded?: boolean;
}
