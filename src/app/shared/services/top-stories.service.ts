import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response, Headers,  } from '@angular/http';
import { baseUrl } from './../baseUrl';
import { ProcessHttpmsgService } from './process-httpmsg.service';
import { ITopStory } from './../interfaces/ITopStory';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class TopStoriesService {

  constructor(
    private http: Http,
    private processHTTPMsgService: ProcessHttpmsgService
  ) { }

  getTopStoriesIds(): Observable<number[]> {
    return this.http.get(baseUrl + 'topstories.json?print=pretty')
    .map(
      (res) => {
        return this.processHTTPMsgService.extractData(res);
    })
    .catch(
      (error) => {
        return this.processHTTPMsgService.handleError(error);
      });
  }

  getItem(id): Observable<ITopStory> {
      return this.http.get(baseUrl + 'item/' + id + '.json?print=pretty')
      .map(
        (res) => {
          return this.processHTTPMsgService.extractData(res);
      })
      .catch(
        (error) => {
          return this.processHTTPMsgService.handleError(error);
      });
    }
}
